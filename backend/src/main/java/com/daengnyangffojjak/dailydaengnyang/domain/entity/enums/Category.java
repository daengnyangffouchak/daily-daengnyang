package com.daengnyangffojjak.dailydaengnyang.domain.entity.enums;

public enum Category {
    HOSPITAL,   //병원
    BEAUTY,     //미용
    TRAVEL,     //여행
    WALK,       //산책
    CARE,       //케어
    DISEASE     //질병
}
