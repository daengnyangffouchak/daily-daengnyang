package com.daengnyangffojjak.dailydaengnyang.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MessageResponse {

	private String msg;

}
